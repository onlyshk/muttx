# This is root Makefile of the muttx project. The
# default target is: 'all'.
#
# Define -DUSE_SMIME_CRYPTO_BACKEND to use smime crypto backend
#
# Define -DUSE_GPGME_CRYPTO_BACKEND to use gpgme crypto backend
#
# Define -DUSE_GSS to use GSS-API
#
# Define -DNO_CRYPT to exclude all crypto backends
#
# Define -DDEBUG to run muttx in debug mode
.PHONY: clean

include Makefile.inc
include Makefile.crypto

OBJECTS += $(CRYPTO_OBJECTS) $(PGP_OBJECTS)					\
	account.o addrbook.o alias.o ascii.o attach.o browser.o			\
	buffy.o charset.o color.o commands.o complete.o compose.o		\
	copy.o curs_lib.o curs_main.o date.o edit.o editmsg.o enter.o		\
	filter.o  flags.o from.o getdomain.o group.o handler.o hash.o		\
	hcache.o hdrline.o headers.o help.o history.o hook.o init.o		\
	keymap.o lib.o mbox.o mbyte.o  menu.o mh.o muttlib.o mutt_sasl.o	\
	mutt_socket.o mutt_ssl.o mutt_tunnel.o mx.o pager.o parse.o pattern.o	\
	postpone.o query.o recvattach.o recvcmd.o resize.o score.o send.o	\
	sendlib.o sidebar.o signal.o smtp.o sort.o status.o system.o		\
	thread.o url.o main.o

DEFS+=$(CRYPTO_DEFS) -DMUTT_VERSION=\"$(MUTT_VERSION)\"
MUTT_INCLUDES+=-I./rfc -I./imap -I./alg -I./alg/data

all: $(LOCAL_LIBS) $(PROGNAME)

$(PROGNAME): $(OBJECTS)
	@$(CC) $(DEFS) $(CFLAGS) $(MUTT_INCLUDES) -o $@ $(OBJECTS) $(LIBS)
	@echo "CC $<"
ifdef DEBUG
	$(STRIP) $(PROGNAME)
endif

$(LIBALG):
	@$(MAKE) -s -C $(ALG_DIR) all

$(LIBRFC):
	@$(MAKE) -s -C $(RFC_DIR) all

$(LIBIMAP):
	@$(MAKE) -s -C $(IMAP_DIR) all

%.o: %.c
	@$(CC) $(DEFS) $(MUTT_INCLUDES) $(CFLAGS) -c -o $@ $<
	@echo "CC $<"

clean:
	@rm -f $(PROGNAME) *.o
	@echo "RM $(PROGNAME)"
	@$(MAKE) -s -C $(DATA_STRUCTS_DIR) clean
	@$(MAKE) -s -C $(ALG_DIR) clean
	@$(MAKE) -s -C $(RFC_DIR) clean
	@$(MAKE) -s -C $(IMAP_DIR) clean
