/*
 * Copyright (C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>
 *
 *     This program is free software; you can redistribute it
 *     and/or modify it under the terms of the GNU General Public
 *     License as published by the Free Software Foundation; either
 *     version 2 of the License, or (at your option) any later
 *     version.
 *
 *     This program is distributed in the hope that it will be
 *     useful, but WITHOUT ANY WARRANTY; without even the implied
 *     warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *     PURPOSE.  See the GNU General Public License for more
 *     details.
 *
 *     You should have received a copy of the GNU General Public
 *     License along with this program; if not, write to the Free
 *     Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *     Boston, MA  02110-1301, USA.
 *
 */

#ifndef _ERRORS_H_
#define _ERRORS_H_

#define RETURN_SUCCESS		 0
#define RETURN_ERR_TERM		 1
#define RETURN_WRONG_ADDR	 2
#define RETURN_ERR_ARG		 3
#define RETURN_ERR_PRIVLG	 4
#define RETURN_ERR_WRONG_MBOX	 5

#endif /* _ERRORS_H_  */
