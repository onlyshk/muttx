/*
 * Copyright (C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef _DATA_BITMAP_H_
#define _DATA_BITMAP_H_

#include "mutt.h"

#define mutt_bit_alloc(n) calloc ((n + 7) / 8, sizeof (char))
#define mutt_bit_set(v,n) v[n/8] |= (1 << (n % 8))
#define mutt_bit_unset(v,n) v[n/8] &= ~(1 << (n % 8))
#define mutt_bit_toggle(v,n) v[n/8] ^= (1 << (n % 8))
#define mutt_bit_isset(v,n) (v[n/8] & (1 << (n % 8)))

#define set_option(x)    mutt_bit_set(options,x)
#define unset_option(x)  mutt_bit_unset(options,x)
#define toggle_option(x) mutt_bit_toggle(options,x)
#define option(x)        mutt_bit_isset(options,x)

unsigned char options[OPTMAX * 8];
unsigned char quad_options[(OPT_MAX * 16)];

#endif /* _DATA_BITMAP_H_ */
