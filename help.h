/*
 * Copyright (C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _HELP_H
#define _HELP_H

#include <sys/utsname.h>

static const char *ReachingUs = "\
To contact the developers, please create issue on\n\
<https://github.com/0xAX/mutt>.";

static const char *Notice = "\
Copyright(C) 1996-2009 Michael R. Elkins and others.\n\
Copyright(C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>\n\n\
This is fork of the mutt, forked by 0xAX\n\n\
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.\n\
Mutt is free software, and you are welcome to redistribute it\n\
under certain conditions; type `mutt -vv' for details.\n";

static const char *Copyright = "\
Copyright(C) 1996-2007 Michael R. Elkins <me@mutt.org>\n\
Copyright(C) 1996-2002 Brandon Long <blong@fiction.net>\n\
Copyright(C) 1997-2008 Thomas Roessler <roessler@does-not-exist.org>\n\
Copyright(C) 1998-2005 Werner Koch <wk@isil.d.shuttle.de>\n\
Copyright(C) 1999-2009 Brendan Cully <brendan@kublai.com>\n\
Copyright(C) 1999-2002 Tommi Komulainen <Tommi.Komulainen@iki.fi>\n\
Copyright(C) 2000-2002 Edmund Grimley Evans <edmundo@rano.org>\n\
Copyright(C) 2006-2009 Rocco Rutte <pdmef@gmx.net>\n\
Copyright(C) 2015-2016 Alexander Kuleshov <kuleshovmail@gmail.com>\n\n\
Many others not mentioned here contributed code, fixes,\n\
and suggestions.\n";

static const char *Licence = "\
    This program is free software; you can redistribute it and/or modify\n\
    it under the terms of the GNU General Public License as published by\n\
    the Free Software Foundation; either version 2 of the License, or\n\
    (at your option) any later version.\n\
\n\
    This program is distributed in the hope that it will be useful,\n\
    but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
    GNU General Public License for more details.\n";
static const char *Obtaining = "\
    You should have received a copy of the GNU General Public License\n\
    along with this program; if not, write to the Free Software\n\
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n\
";

static void mutt_usage(void)
{
	puts(mutt_make_version());

	puts(
		"usage: mutt [<options>] [-z] [-f <file> | -yZ]\n\
       mutt [<options>] [-x] [-Hi <file>] [-s <subj>] [-bc <addr>] [-a <file> [...] --] <addr> [...]\n\
       mutt [<options>] [-x] [-s <subj>] [-bc <addr>] [-a <file> [...] --] <addr> [...] < message\n\
       mutt [<options>] -p\n\
       mutt [<options>] -A <alias> [...]\n\
       mutt [<options>] -Q <query> [...]\n\
       mutt [<options>] -D\n\
       mutt -v[v]\n");

	puts("\
options:\n\
  -A <alias>\texpand the given alias\n\
  -a <file> \tattach file to the message\n\
  --attachlist <files> attach files to the message\n\
  -b <address>\tspecify a blind carbon-copy(BCC) address\n\
  -c <address>\tspecify a carbon-copy(CC) address\n\
  -D\t\tprint the value of all variables to stdout");

	puts(
		"  -e <command>\tspecify a command to be executed after initialization\n\
  -f <file>\tspecify which mailbox to read\n\
  -F <file>\tspecify an alternate muttrc file\n\
  -H <file>\tspecify a draft file to read header and body from\n\
  -i <file>\tspecify a file which Mutt should include in the body\n\
  -m <type>\tspecify a default mailbox type\n\
  -p\t\trecall a postponed message");

	puts("\
  -Q <variable>\tquery a configuration variable\n\
  -R\t\topen mailbox in read-only mode\n\
  -s <subj>\tspecify a subject(must be in quotes if it has spaces)\n\
  -v\t\tshow version and LICENSE information\n\
  -vv\t\tshow version COPYRIGHT and LICENSE information\n\
  -x\t\tsimulate the mailx send mode\n\
  -y\t\tselect a mailbox specified in your `mailboxes' list\n\
  -z\t\texit immediately if there are no messages in the mailbox\n\
  -Z\t\topen the first folder with new message, exit immediately if none\n\
  -h\t\tthis help message");

	exit(RETURN_SUCCESS);
}

static inline void show_version(void)
{
	struct utsname uts;

	puts(mutt_make_version());
	puts((Notice));
	uname(&uts);
	puts((ReachingUs));
}

static inline void show_version_verbose(void)
{
	puts(mutt_make_version());
	puts((Copyright));
	puts((Licence));
	puts((Obtaining));
	puts((ReachingUs));
}

#endif /* _HELP_H */
